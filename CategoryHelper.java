public class CategoryHelper extends HelperBase {

    public CategoryHelper(ApplicationManager manager) {
        super(manager);
    }
    public void addCategory(String name) {
        manager.navigationHelper.goToLink("Добавить категорию");
        fillTheField("cat_name", name);
        manager.navigationHelper.clickButton("submit");
    }
}
