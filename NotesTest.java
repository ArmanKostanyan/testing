import org.testng.annotations.*;

@Test
public class NotesTest extends AuthBase {

    @Test
    public void notesTest() throws Exception {
        manager.navigationHelper.goToHomePage();
        manager.loginHelper.login(new AccountData("test", "test"));
        Note note1 = new Note("Моя первая заметка");
        manager.noteHelper.addNote(note1);
        note1.setCategory(7);
        manager.noteHelper.editCategory(note1);
        manager.noteHelper.deleteNote();
        manager.categoryHelper.addCategory("Category1");
        manager.loginHelper.logout();
    }
}
