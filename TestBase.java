import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.fail;

public class TestBase {
    protected ApplicationManager manager;
    protected StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        manager = new ApplicationManager();
        manager.driver.manage().window().maximize();
        manager.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        manager.navigationHelper.goToHomePage();
        manager.loginHelper.login(new AccountData("test", "test"));
    }

    @AfterClass(alwaysRun = true)
    protected void tearDown() throws Exception {
        manager.stop();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}
