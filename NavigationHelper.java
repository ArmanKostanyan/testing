import org.openqa.selenium.By;

public class NavigationHelper extends HelperBase {
    private String baseUrl;

    public NavigationHelper(ApplicationManager manager, String baseUrl) {
        super(manager);
        this.baseUrl = baseUrl;
    }

    public void goToLink(String link) {
        driver.findElement(By.linkText(link)).click();
    }


    public void clickButton(String name) {
        driver.findElement(By.name(name)).click();
    }

    public void goToHomePage() {
        driver.get(baseUrl + "/");
    }

}
