import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.fail;

public class ApplicationManager {

    private String baseUrl;
    protected Settings settings;
    protected WebDriver driver;
    protected LoginHelper loginHelper;
    protected NavigationHelper navigationHelper;
    protected NoteHelper noteHelper;
    protected CategoryHelper categoryHelper;
    protected StringBuffer verificationErrors = new StringBuffer();

    public ApplicationManager(){
        driver = new FirefoxDriver();
        settings = new Settings();
        baseUrl = settings.getBaseUrl();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        navigationHelper = new NavigationHelper(this, baseUrl);
        loginHelper = new LoginHelper(this);
        noteHelper = new NoteHelper(this);
        categoryHelper = new CategoryHelper(this);
    }

    @AfterClass(alwaysRun = true)
    protected void stop() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}
