public class LoginHelper extends HelperBase{

    public LoginHelper(ApplicationManager manager) {
        super(manager);
    }
    public void login(AccountData accountData) {
        fillTheField("username", accountData.getUsername());
        fillTheField("password", accountData.getPassword());
        manager.navigationHelper.clickButton("login");
    }

    public void logout() {
        manager.navigationHelper.goToLink("Выход");
    }
}
