import org.openqa.selenium.By;

public class NoteHelper extends HelperBase{

    public NoteHelper(ApplicationManager manager){
        super(manager);
    }

    public void addNote(Note note) {
        fillTheField("nazv_zametki", note.getName());
        manager.navigationHelper.clickButton("submit");
    }


    public void editNote() {
        driver.findElement(By.className("note_buttons")).findElement(By.tagName("a")).click();
    }

    public void editCategory(Note note) {
        editNote();
        driver.findElements(By.className("select_category_block")).get(note.getCategory()).click();
        manager.navigationHelper.clickButton("submit");
    }

    public void deleteNote() {
        driver.findElement(By.className("note_buttons")).findElement(By.className("item_delete")).click();
    }
}
