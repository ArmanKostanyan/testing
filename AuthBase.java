import org.testng.annotations.BeforeClass;

public class AuthBase extends TestBase {
    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        manager.loginHelper.login(new AccountData(manager.settings.getUsername(), manager.settings.getPassword()));
    }
}
